const fs = require(`fs`);
const path = require('path');

let doNotCopy = ["navbar", "footer", "info-panel", "image-viewer"];

function copyFileSync( source, target ) {

    var targetFile = target;

    //if target is a directory a new file with the same name will be created
    if ( fs.existsSync( target ) ) {
        if ( fs.lstatSync( target ).isDirectory() ) {
            targetFile = path.join( target, path.basename( source ) );
        }
    }

    fs.writeFileSync(targetFile, fs.readFileSync(source));
}

function copyFolderRecursiveSync( source, target ) {
    var files = [];

    //check if folder needs to be created or integrated
    var targetFolder = path.join( target, path.basename( source ) );
    if ( !fs.existsSync( targetFolder ) ) {
        fs.mkdirSync( targetFolder );
    }

    //copy
    if ( fs.lstatSync( source ).isDirectory() ) {
        files = fs.readdirSync( source );
        files.forEach( function ( file ) {
            var curSource = path.join( source, file );
            if ( fs.lstatSync( curSource ).isDirectory() ) {
                copyFolderRecursiveSync( curSource, targetFolder );
            } else {
                copyFileSync( curSource, targetFolder );
            }
        } );
    }
}

const deleteFolderRecursive = function(p) {
    if (fs.existsSync(p)) {
      fs.readdirSync(p).forEach((file, index) => {
        const curPath = path.join(p, file);
        if (fs.lstatSync(curPath).isDirectory()) { // recurse
          deleteFolderRecursive(curPath);
        } else { // delete file
          fs.unlinkSync(curPath);
        }
      });
      fs.rmdirSync(p);
    }
  };

function insertValues(contents, texts) {
    return contents.replace(/\{\{(\.?[a-zA-Z0-9\-]*\.?)*\}\}/g, replacement => {
        let keys = replacement.slice(2,-2).split(`.`);
        if (keys[0] != ``)
        {
            let value = texts;
            try {
                keys.forEach(key => {
                    value = value[key];
                });
                if (value == undefined) {
                    throw ``;
                }
            } catch (e) {
                return replacement;
            }
            return value.replace(/\n/g,`<br/>`);
        } else {
            return insertValues(texts[`_files`][keys[1]], texts);
        }
    });
}

function scanDirectory(dir, locale, texts) {
    fs.readdir(`./src/${dir}`, (err, files) => {
        files.forEach(file => {
            if (file == `dist`) {
                copyFolderRecursiveSync(`./src/dist`,`./html/`);
            } else if (fs.lstatSync(`./src/${dir}${file}`).isDirectory()) {
                scanDirectory(`${dir}${file}/`, locale, texts);
                fs.mkdirSync(`./html/${locale}/${dir}${file}`)
            } else if (file != `main-index.html`) {
                let contents = fs.readFileSync(`./src/${dir}${file}`, `utf-8`);
                texts[`_files`][`dir`] = `/${dir}${file}`.replace(`index.html`,``);
                texts[`_files`][`title`] = texts[`titles`][`${dir}${file}`.replace(`index.html`,``).replace(`.html`,``)];
                
                contents = insertValues(contents, texts);
                fs.writeFileSync(`./html/${locale}/${dir}${file}`,contents);
            }
        });
    });
}

fs.readdir(`./locales/`, (err, files) => {
    deleteFolderRecursive(`./html`);
    fs.mkdirSync(`./html`);
    files.forEach(file => {
        let locale = file.split(`.`)[0];
        let texts = JSON.parse(fs.readFileSync(`./locales/${file}`, `utf-8`));
        texts[`_files`] = {};
        doNotCopy.forEach(item => {
            texts[`_files`][item] = fs.readFileSync(`./src/${item}.html`, `utf-8`);
        });
        
        fs.mkdirSync(`./html/${locale}`);
        scanDirectory(`./`,locale,texts);
    });
});