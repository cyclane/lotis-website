#!/bin/bash
WIDTH=360;

for file in src/dist/images/gallery/*; do
    height=$(identify -format "%h" "$file");
    width=$(identify -format "%w" "$file");
    
    new_height=$((height/(width / WIDTH)));
    convert -resize "$WIDTH""x$new_height" "$file" "${file%.*}.compressed.${file#*.}" 
done