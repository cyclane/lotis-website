function goToSlide(n) {
    $(`.slideshow-container figure.active`).removeClass(`active`);
    $(`.slideshow-container .text div.active`).removeClass(`active`);
    $(`.slideshow-container .text.active`).removeClass(`active`);

    $(`.slideshow-container figure#image-${n}`).addClass(`active`);
    $(`.slideshow-container .text div#text-${n}`).addClass(`active`);
    $(`.slideshow-container .text div#text-${n}`).parent().addClass(`active`)
}

$(document).ready(() => {
    const timeLoad = (new Date()).getTime()/1000;
    var currentSlide = 1;
    let intervalId;

    for (var i=1; i<=3; i++) {
        $(`a#slide-${i}`).click((e) => {
            let selected = Number(e.target.id.slice(-1));
            /* Old
            let newDelay = -1 * ((selected-1)*10 + 4 - ((new Date()).getTime()/1000-timeLoad)%30); // Calculates the delay required for the animation to shift it to the correct position
            document.documentElement.style.setProperty(`--delay`, newDelay+`s`);
            */
            goToSlide(selected);
            currentSlide = selected;
            clearInterval(intervalId);
            intervalId = setInterval(() => {
                currentSlide = (currentSlide)%3 + 1;
                goToSlide(currentSlide);
            }, 10000);
        });
    }

    goToSlide(currentSlide);

    intervalId = setInterval(() => {
        currentSlide = (currentSlide)%3 + 1;
        goToSlide(currentSlide);
    }, 10000);
});