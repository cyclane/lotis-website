function setInfoPannel(element) {
    let infoSection = $("section.info");

    let title = $("section.image #"+element.id).find("h4").text();
    let description = $("section.image #"+element.id).find("p").text();
    let index = $(element).index();
    let maxIndex = $(element).parent().find("span").length;
    if (infoSection.find(".video").length > 0) {
        infoSection.find(".video").remove();
    }
    if ($("section.image #"+element.id).find(".video").length == 0) {
        infoSection.find("img").attr("src", "/dist/images/services/"+element.id+".jpg");
    } else {
        infoSection.find("img").attr("src", "");
        infoSection.find("img").after(`<div class="video">${$("section.image #"+element.id).find(".video").html()}</div>`)
    }
    infoSection.find("h2").html(title);
    infoSection.find("p.description").html(description);
    infoSection.find(" div#controls p").html(("0"+index).slice(-2) + " / " + ("0"+maxIndex).slice(-2));

    if (index == 1) {
        infoSection.find("a#previous").css({
            "pointer-events": "none",
            "opacity": "0.5"
        })
    } else {
        infoSection.find("a#previous").css({
            "pointer-events": "all",
            "opacity": "1"
        })
    }

    if (index == maxIndex) {
        infoSection.find("a#next").css({
            "pointer-events": "none",
            "opacity": "0.5"
        })
    } else {
        infoSection.find("a#next").css({
            "pointer-events": "all",
            "opacity": "1"
        })
    }

    infoSection.addClass("open");
    $("div.info-close-area").addClass("open");
}


function onYouTubePlayerAPIReady() {
    let videos = ["video1", "video2"];
    videos.forEach(video => {
        let player = new YT.Player(video, {
            events: {
                "onStateChange": (data) => {
                    console.log(data);
					let sel = $("section.text .embed iframe#"+video);
                    if (data.data == 0) {
						sel.prevAll().css("opacity", "1");
                    } else if (data.data === 3) {
						sel.prevAll().css("opacity", "0");
                    }
                }
            }
        });
    });
}


class ImageViewer {
    constructor(containerElement, imageLinksContainer, imageSources, useCompressedThumbnails) {
        this.containerElement = containerElement;
        this.imageLinksContainer = imageLinksContainer;
        this.useCompressedThumbnails = useCompressedThumbnails;
        this.current = -1;

        // Close by clicking off window
        this.containerElement.click((event) => {
            if (event.target == this.containerElement[0]) {
                this.close();
            }
        });

        // Close button
        containerElement.find(".viewer-window a#close").click(() => {
            this.close();
        });

        // Find all the triggers and assign them to sources
        let children = [];
        for (let i = 1; i <= (imageLinksContainer.find(`a`).length / imageLinksContainer.length); i++) {
            children.push({
                element: $(imageLinksContainer.find(`a`)[i-1]),
                source: imageSources[i-1]
            });
        }
        children.forEach((child, index) => {
            child.element.click(() => {
                this.open(index);
            });
        });
        this.children = children;

        // Resize the window correctly for image
        this.containerElement.find(".viewer-window .image-container img").bind("load", event => {
            this.containerElement.find(".viewer-window .image-container").width("auto");
            this.containerElement.find(".viewer-window .image-container").height("auto");
            this.containerElement.find(".viewer-window div#image-select").width("auto");
            
            let containerWidthHeightRatio = event.target.width / event.target.height;
            let bodyWidthHeightRatio = $(window).width() / $(window).height();

            var width;
            var height;

            if (containerWidthHeightRatio > bodyWidthHeightRatio) {
                width = Math.round($(window).width() * 0.8);
                height = Math.round((1/containerWidthHeightRatio)*width);
            } else {
                height = Math.round($(window).height() * 0.8);
                width = Math.round(containerWidthHeightRatio*height);
            }

            this.containerElement.find(".viewer-window .image-container").width(width);
            this.containerElement.find(".viewer-window .image-container").height(height);
            this.containerElement.find(".viewer-window div#image-select").width(width);
        });

        // Create image selection bar
        var barHtml = "";
        imageSources.forEach((source, index) => {
            var finalSource = source;
            if (this.useCompressedThumbnails) {
                finalSource = source.split(".").join(".compressed.");
            }
            barHtml += `<a><img src=${finalSource} alt=#${index+1}/></a>`;
        });
        this.containerElement.find(".viewer-window div#image-select").html(barHtml);

        // Assign events to selection bar
        for (let i=1; i <= imageSources.length; i++) {
            this.containerElement.find(`.viewer-window div#image-select a:nth-child(${i})`).click(() => {
                this.open(i-1);
            });
        }

        // Next and previous buttons
        this.containerElement.find(".viewer-window .image-container a#previous").click(() => {
            this.previous();
        });
        this.containerElement.find(".viewer-window .image-container a#next").click(() => {
            this.next();
        });
    }
    previous() {
        let maxIndex = this.children.length;
        this.current = (this.current + maxIndex - 1)%maxIndex;
        this.open(this.current);
    }
    next() {
        let maxIndex = this.children.length;
        this.current = (this.current + 1)%maxIndex;
        this.open(this.current);
    }
    open(index) {
        let maxIndex = this.children.length;
        this.containerElement.find(".viewer-window p").html(("0"+(index+1)).slice(-2) + " / " + ("0"+maxIndex).slice(-2));
        this.containerElement.find(".viewer-window .image-container img").attr("src", this.children[index].source);
        
        this.current = index;
        this.containerElement.removeClass("disabled");
        setTimeout(() => {this.containerElement.removeClass("hidden")}, 1); // Avoid DOM reflow removing transition

        document.onkeydown = (event) => {
            switch (event.keyCode) {
                case 37:
                    this.previous();
                    break;
                case 39:
                    this.next();
                    break;
            }
        };
    }
    close() {
        this.containerElement.addClass("hidden");
        setTimeout(() => {
            this.containerElement.addClass("disabled");
        }, 200);
    }
}


$(document).ready(() => {
    var tag = document.createElement("script");
    tag.src = "https://www.youtube.com/player_api";
    var firstScriptTag = document.getElementsByTagName("script")[0];
    firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);

    let currentlyActive = null;

    // Mobile navbar opening event
    $("nav#mobile .dropdown-icon").click(() => {
        $("nav#mobile").toggleClass("open");
        $(".mobile-dropdown").toggleClass("open");
    });

    // Mobile navbar services dropdown opening event
    $(".mobile-dropdown h2").click(() => {
        $(".mobile-dropdown").toggleClass("services-open");
    });

    // Hover event for image pointers
    $("section.image span").hover(() => {
        $("section.image").addClass("open");
    }, () => {
        if (!$("section.image").hasClass("force-open")) {
            $("section.image").removeClass("open");
        }
    });

    // Click event for image pointers
    $("section.image span").click((event) => {
        let infoSection = $("section.info");
        let imageSections = $("section.image");
        let targetElement = $("section.image #"+event.target.id);

        if (currentlyActive == null) { // Open
            imageSections.addClass("force-open");
            targetElement.addClass("open");
            currentlyActive = event.target.id;
        } else if (currentlyActive == event.target.id) { // Close
            imageSections.removeClass("force-open");
            targetElement.removeClass("open");
            infoSection.removeClass("open");
            $("div.info-close-area").removeClass("open");
            currentlyActive = null;
        } else { // Switch
            $("section.image #"+currentlyActive).removeClass("open");
            targetElement.addClass("open");
            currentlyActive = event.target.id;
        }
        
        if (currentlyActive != null) {
            setInfoPannel(event.target);
        }
    });

    // Info panel close button
    let closeInfo = () => {
        $("section.image").removeClass("force-open");
        $("section.image").removeClass("open");
        $("section.image span").removeClass("open");
        $("section.info").removeClass("open");
        $("div.info-close-area").removeClass("open");
        currentlyActive = null;
    };
    $("section.info a.info-close").click(closeInfo);
    $("div.info-close-area").click(closeInfo);

    // Info panel navigation controls
    $("section.info a#previous").click(() => {
        let currentIndex = $("section.image #"+currentlyActive).index();
        let newActive = $("section.image #"+currentlyActive).parent().children()[currentIndex-1];
        
        $("section.image #"+currentlyActive).removeClass("open");
        $("section.image #"+newActive.id).addClass("open");
        currentlyActive = newActive.id;

        setInfoPannel(newActive);
    });
    $("section.info a#next").click(() => {
        let currentIndex = $("section.image #"+currentlyActive).index();
        let newActive = $("section.image #"+currentlyActive).parent().children()[currentIndex+1];
        
        $("section.image #"+currentlyActive).removeClass("open");
        $("section.image #"+newActive.id).addClass("open");
        currentlyActive = newActive.id;

        setInfoPannel(newActive);
    });

    // Video embed animation
    $("section.text .embed *").hover(() => {
        $("section.text .embed div").addClass("hover");
    }, () => {
        $("section.text .embed div").removeClass("hover");
    });

    // Navbar image viewer initialisation
    let imageViewer = new ImageViewer($(".navbar-image-viewer"), $(".standard-links"),
        ["/dist/images/ISO-9001-en.jpg", "/dist/images/ISO-14001-en.jpg", "/dist/images/OHSAS-18001-en.jpg"]
    );
});
