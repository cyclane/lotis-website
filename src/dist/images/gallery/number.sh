index=1

for filename in $1/*.jpg; do
    echo $index, $filename
    mv "$filename" "$1/$index.jpg"
    ((index++))
done
