for filename in $1/*.jpg; do
    echo $filename
    convert "$filename" -resize 240x "${filename/\./\.compressed\.}"
done